import { Container,Nav, Navbar } from 'react-bootstrap';

const TopBar = () => {
	return (
		<>
			<Navbar bg="primary" variant="dark">
        <Container>
          <Navbar.Brand href="#home">F2-CLASS</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
          </Nav>
        </Container>
      </Navbar>	
		</>
	);
}

export default TopBar;
