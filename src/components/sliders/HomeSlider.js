import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import './HomeSlider.css';
import img1 from "../../images/img1.png";
import img2 from '../../images/img2.png';
import img3 from '../../images/img3.png';

class HomeSlider extends Component {

  sliders = this.props.sliders;
  render() {

    return (
        <>
          <Carousel variant="dark">
            {this.sliders && this.sliders.map((item, key) => (
              <Carousel.Item key={key} className="custom-slider-image">
              <img
                className="d-block w-100"
                src={item.img.src}
                alt={item.img.alt}
              />
              <Carousel.Caption>
                <h5>{item.caption.title}</h5>
                <p>{item.caption.description}</p>
              </Carousel.Caption>
            </Carousel.Item>
            ))}
          </Carousel>  
        </>
    );
  }
}

export default HomeSlider;
