import logo from './logo.svg';
import './App.css';
import Button from 'react-bootstrap/Button';
import TopBar from './components/navbars/TopBar';
import HomeSlider from './components/sliders/HomeSlider';
import './components/sliders/HomeSlider.css';
import img1 from "./images/img1.png";
import img2 from './images/img2.png';
import img3 from './images/img3.png';

function App() {

  const sliders = [
    {img: {src: img1, alt: "Image1"}, caption: {title: "Titre de image 1", description: "Description de image 1"}},
    {img: {src: img2, alt: "Image2"}, caption: {title: "Titre de image 2", description: "Description de image 2"}},
    {img: {src: img3, alt: "Image3"}, caption: {title: "Titre de image 3", description: "Description de image 3"}}
  ];

  return (
    <div className="Apps">
        <TopBar></TopBar>
        <div className="custom-slider-image">
          <HomeSlider sliders={sliders}></HomeSlider>
        </div>
    </div>
  );
}

export default App;
